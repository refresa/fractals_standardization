# --- FRACTAL STANDARDISATION: 01 - rating questionnaire------------------------

# read in rating questionnaire data
# save aggregated data
# descritpive statistics 
# correlations & scatterplots

# Rebecca Ovalle Fresa rebecca.ovallefresa@gmail.com
# first try: 2020-06-02
# last changes (adjust figures): 2021-04-09

# latest run with:
# R version 4.0.4 (2021-02-15)
# Platform: x86_64-pc-linux-gnu (64-bit)
# Running under: Manjaro Linux


# -------------------------------------------------------- clear and inform ----
#
# ******************************************************************************

# clear console
cat('\014')

# clear workspace
rm(list=ls())

# clear plotspace (if it is not already empty)
if (!is.null(dev.list())) {
  dev.off(dev.list()['RStudioGD'])
}

# check R version
sessionInfo()

# set output options
options(scipen = 100)
options(digits = 5)

# decide to see & save plots (1 = yes, 0 = no)
plotspace = 0
saveplots = 0


# ----------------------------------------------------------- load packages ----
#
# ******************************************************************************

# library(rstudioapi) # get path (directly loaded in script)
# library(GGally)     # correlation plots (directly loaded in script)

library(tidyverse)    # collection of R packages
library(gridExtra)    # order graphs in a grid
library(apaTables)    # creates APA formatted tables
library(schoRsch)     # APA-formatting of t-tests or ANOVAS
library(ggbeeswarm)   # beeswarm plot


# -------------------------------------------------------------- directories ----
#
# ******************************************************************************

# set working directory
dir  <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
dir  <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

dir.i <- paste0(substring(dir, 1, nchar(dir)-7), '/inR/')

# create directories for figure files and data output files
dir.f <- paste0(substring(dir, 1, nchar(dir)-7), '/figures/')
dir.o <- paste0(substring(dir, 1, nchar(dir)-7), '/outR/')

# create subdirectories if they do not exist
dir.create(dir.f, showWarnings = FALSE)
dir.create(dir.o, showWarnings = FALSE)


# ------------------------------------------------------------- import data ----
#
# ******************************************************************************

frac <- read.csv(paste0(dir.i, 'fractals_rating_questionnaire_data.csv'))


# -------------------------------------------------- descriptive statistics ----
#
# ******************************************************************************

summary_frac <- frac %>%
  group_by(fractal, feature) %>%
  summarize(avg = mean(ratingValue), SD = sd(ratingValue), Min = min(ratingValue),
              Max = max(ratingValue))


desc_frac <- summary_frac %>% group_by(feature) %>%
  summarize(M = mean(avg), SD = sd(avg), Min = min(avg), Max = max(avg), N =n())


# --- plot
tmp <- summary_frac %>%
  mutate(feature = str_to_title(feature),
         feature = as.factor(feature))

frac_descr_violin <- ggplot(tmp, aes(x = feature, y = avg)) +
  geom_boxplot(outlier.shape =NA, size = 1) + # do not show outliers
  geom_quasirandom(alpha = 0.5, groupOnX = TRUE) +
  theme_minimal() +
  labs(fill = 'Average', x = '\nFeature', y = 'Rating Value\n') +
  theme(legend.position = 'none',
        axis.text=element_text(size=30),
        axis.title = element_text(size = 40)
      )

if (saveplots == 1) {
  x <-  20
  y <-  12
  ggsave(file = paste0('descriptives_questionnaire_violinplot_', Sys.Date(), '.pdf'),
         plot = frac_descr_violin, path = dir.f, width = x, height = y)
}


# --------------------------------------------- correlations & scatterplots ----
#
# ******************************************************************************

# --- inter item correlations ---
# wide dataformat
frac_wd <- summary_frac %>%
  select(fractal, feature, avg) %>%
  spread(feature, avg) %>%
  mutate(animacy = liveliness, favorableness = favourableness) %>%
  select(fractal, abstractness, animacy, complexity, familiarity, favorableness,
          memorability, verbalizability)

# save file with aggregated data
write_csv(frac_wd, paste0(dir.o, 'fractals_rating_questionnaire_avg', '.csv'))

# correlation matrix
param_cors <- apa.cor.table(frac_wd, 1, filename = paste0(dir.o, 'averaged_norms_correlations.doc'))

# seperate correlations if needed (do for each pair...)
corr <- cor.test(frac_wd$favorableness, frac_wd$complexity, method = 'pearson')
cor_out(corr)


# nicer scatterplots of all correlations including abstractness and verbalizability
tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'animacy'))
scpl1 <- ggplot(data = tmp, aes(x = abstractness, y = animacy)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'animacy\n')

tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'complexity'))
scpl2 <- ggplot(data = tmp, aes(x = abstractness, y = complexity)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'complexity\n')

tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'familiarity'))
scpl3 <- ggplot(data = tmp, aes(x = abstractness, y = familiarity)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'familiarity\n')

tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'favorableness'))
scpl4 <- ggplot(data = tmp, aes(x = abstractness, y = favorableness)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'favorableness\n')

tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'memorability'))
scpl5 <- ggplot(data = tmp, aes(x = abstractness, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'memorability\n')

tmp <- frac_wd %>% select(c('fractal', 'abstractness', 'verbalizability'))
scpl6 <- ggplot(data = tmp, aes(x = abstractness, y = verbalizability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nabstractness', y = 'verbalizability\n')

tmp <- frac_wd %>% select(c('fractal', 'verbalizability', 'animacy'))
scpl7 <- ggplot(data = tmp, aes(x = verbalizability, y = animacy)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nverbalizability', y = 'animacy\n')

tmp <- frac_wd %>% select(c('fractal', 'verbalizability', 'complexity'))
scpl8 <- ggplot(data = tmp, aes(x = verbalizability, y = complexity)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nverbalizability', y = 'complexity\n')

tmp <- frac_wd %>% select(c('fractal', 'verbalizability', 'familiarity'))
scpl9 <- ggplot(data = tmp, aes(x = verbalizability, y = familiarity)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nverbalizability', y = 'familiarity\n')

tmp <- frac_wd %>% select(c('fractal', 'verbalizability', 'favorableness'))
scpl10 <- ggplot(data = tmp, aes(x = verbalizability, y = favorableness)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nverbalizability', y = 'favorableness\n')

tmp <- frac_wd %>% select(c('fractal', 'verbalizability', 'memorability'))
scpl11 <- ggplot(data = tmp, aes(x = verbalizability, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nverbalizability', y = 'memorability\n')

tmp <- frac_wd %>% select(c('fractal', 'complexity', 'animacy'))
scpl12  <- ggplot(data = tmp, aes(x = complexity, y = animacy)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\ncomplexity', y = 'animacy\n')

tmp <- frac_wd %>% select(c('fractal', 'complexity', 'familiarity'))
scpl13 <- ggplot(data = tmp, aes(x = complexity, y = familiarity)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\ncomplexity', y = 'familiarity\n')

tmp <- frac_wd %>% select(c('fractal', 'complexity', 'favorableness'))
scpl14  <- ggplot(data = tmp, aes(x = complexity, y = favorableness)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\ncomplexity', y = 'favorableness\n')

tmp <- frac_wd %>% select(c('fractal', 'complexity', 'memorability'))
scpl15  <- ggplot(data = tmp, aes(x = complexity, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\ncomplexity', y = 'memorability\n')

tmp <- frac_wd %>% select(c('fractal', 'familiarity', 'animacy'))
scpl16  <- ggplot(data = tmp, aes(x = familiarity, y = animacy)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nfamiliarity', y = 'animacy\n')

tmp <- frac_wd %>% select(c('fractal', 'familiarity', 'favorableness'))
scpl17  <- ggplot(data = tmp, aes(x = familiarity, y = favorableness)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nfamiliarity', y = 'favorableness\n')

tmp <- frac_wd %>% select(c('fractal', 'familiarity', 'memorability'))
scpl18  <- ggplot(data = tmp, aes(x = familiarity, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nfamiliarity', y = 'memorability\n')

tmp <- frac_wd %>% select(c('fractal', 'favorableness', 'animacy'))
scpl19  <- ggplot(data = tmp, aes(x = favorableness, y = animacy)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nfavorableness', y = 'animacy\n')

tmp <- frac_wd %>% select(c('fractal', 'favorableness', 'memorability'))
scpl20  <- ggplot(data = tmp, aes(x = favorableness, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nfavorableness', y = 'memorability\n')

tmp <- frac_wd %>% select(c('fractal', 'animacy', 'memorability'))
scpl21  <- ggplot(data = tmp, aes(x = animacy, y = memorability)) +
  geom_point(stat = 'identity', alpha = 0.6) +
  geom_smooth(method = lm, se = FALSE) +
  xlim(0, 6.2) +
  ylim(0, 6.2) +
  theme_bw() +
  theme(text = element_text(size = 20)) +
  labs(x = '\nanimacy', y = 'memorability\n')

corr.abstr.arranged <- grid.arrange(scpl1, scpl2, scpl3, scpl4, scpl5, scpl6, scpl7,
                                    scpl8, scpl9, scpl10, scpl11, scpl12, scpl13,
                                    scpl14, scpl15, scpl16, scpl17, scpl18, scpl19,
                                    scpl20, scpl21, nrow = 6)

if (saveplots == 1) {
  x <-  17.5
  y <- 25
  ggsave(file = paste0('scatterplots_questionnaire_', Sys.Date(), '.pdf'),
         plot = corr.abstr.arranged, path = dir.f, width = x, height = y)
}

# --------------------------------------------------------------------- END ----
#
# ******************************************************************************
