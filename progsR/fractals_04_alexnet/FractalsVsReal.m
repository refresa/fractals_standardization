% http://deeplearning.net/datasets/
% caltec 256, cifar, ImageNet are good starting points I guess...

addpath('~/projects/alexnettest/')

dirs = {'~/projects/alexnettest/real/'; % BOSS
        '~/projects/alexnettest/fractals/';% Rebeccas Fractals
        '~/projects/alexnettest/objects4thomas/'; % A subset of M. Hebarts THINGS database
        '~/projects/alexnettest/NormalizedBrodatz/'}; % as suggested by Reviewer 1


setnames = {'Boss', ...
         'Fractals', ...
         'THINGS', ...
         'Brodatz'};

filetypes = {'jpg';
             'bmp';
             'jpg';
             'png'};

for d = 1:numel(dirs)
    
    imnames = dir([dirs{d} '*.' filetypes{d}]);
    imnames = imnames(~[imnames(:).isdir]);
        
    disp(sprintf('workign on %s', dirs{d}));
    
    for i = 1:numel(imnames)
        [label, confidence] = classifyImage(...
            [imnames(i).folder,filesep, imnames(i).name]);
        
        % keep highest confidence
        
        distributions{d}(i) = confidence(1);
        labels{d}{i} = label{1};
        origname{d}{i} = imnames(i).name;
        fprintf('%d of %d\n',i, numel(imnames));
    end
    disp(' ');
end



for d = 1:numel(dirs)
    tab{d} = table(labels{d}', origname{d}', distributions{d}', ...
                   repmat(setnames(d), numel(labels{d}),1));

end
all = vertcat(tab{3}, tab{2}, tab{1});
all.Properties.VariableNames = {'alexnetOutput' 'Filename' 'Confidence' 'Database'};

writetable(all, 'confidences.xlsx');


figure('Color', 'w')

[y,x,g] = iosr.statistics.tab2box(all.Database,all.Confidence);

    h = iosr.statistics.boxPlot(x,y,...
                                'scalewidth',false,'xseparator',true,...
                                'groupLabels',g,'showLegend',true, ...
                                'theme', 'colorlines', ... %'colorall' 'default'
                                'sampleSize', false, ...
                                'showLegend', false, ...
                                'notch', false, ...
                                'outlierSize', 3, ...
                                'symbolMarker', 'x',...
                                'showScatter', true,...
                                'scatterMarker', '.', ...
                                'scatterSize', 3, ...
                                'showViolin', false, ...
                                'lineWidth', 0.3 ...
                                );

figure('Color', 'w')
for d = [1 2 3]%numel(dirs)
    histogram(distributions{d}, 30, 'Normalization', 'probability'); hold on
end