# AlexNetOnFractals

This repository contains matlab scripts to run alexnet on on the images reported in Ovalle et al. in Prep.

The images are listed in the corresponding text files and can be downloaded on:

- fractal_list.txt - https://osf.io/ckfmv/?view_only=1fc5eb8dfeee40e89d3e5d2eb4507c90 (permanent link for now)
- boss_list.txt - https://drive.google.com/drive/folders/1FpnEFkbqe_huRwfsCf7gs5R1zuc1ZOkn
- things_list.txt - : https://osf.io/jum2f/ (DOI 10.17605/OSF.IO/JUM2F)
- brodatz_list.txt - http://multibandtexture.recherche.usherbrooke.ca/normalized_brodatz.html

