%% this is wrapper function around darknet alexnet shellscripts
%% make sure to install darkent and alexnet weights first: https://pjreddie.com/darknet/imagenet/

function [label confidence] =  classifyImage(impath)
darknetinstallpath = '~/scripts/darknet/';
P_W_D = pwd;
cd(darknetinstallpath);


command = sprintf(['./darknet classifier predict cfg/imagenet1k.data ' ...
                'cfg/alexnet.cfg alexnet.weights %s'],impath);
[status out] = system(command);
out = splitlines(out);

id = 0;

for ol = 1:numel(out)
    
    idx_ = strfind(out{ol}, '%');
    if ~isempty(idx_)
        id = id +1;
        label{id} = out{ol}(idx_(1)+3:end);
        confidence(id) = str2num(out{ol}(1:idx_(1)-1));
    end
end

assert(~isempty(label));
assert(~isempty(confidence));

cd(P_W_D);



