# Fractals: Analyses

Scripts for the analyses reported in
Ovalle Fresa et al. (under review). Standardized database of 400 complex abstract fractals

For questions and comments, please write an email: rebecca.ovallefresa@gmail.com

The R scripts were run in a folder system as follows: 
- main folder (any name)
	+ subfolder 'inR': contains the input files, with 
		+ a separate subfolder 'labeling' for the processing and analyses of the labeling questionnaire
	+ subfolder 'progsR': contains the R scripts and the
		+ subfolder 'functions'
	+ subfolders for output will be created during analyses: 'figures' for figures and 'outR' for all other output files. 
		+ In both subfolders, a separate subfolder 'labeling' will be created for the output files from the analyses of the labeling questionnaire.


### fractals_01_rating_questionnaire.R

- R script
- input:
	+ datafile (fractals_rating_questionnaire_data.csv)
- aggregating
- descriptive statistics and correlation analyses
- output:
	+ aggregated datafile (fractals_rating_questionnaire_avg.csv)
	+ violin plot, scatterplots, correlation matrix
	
	
### fractals_02_labeling_questionnaire

- subfolder with R scripts for processing and analyses of the labeling questionnaire (frct_labeling):

#### frct_labeling_00_prep_corpora.R

- R script
- input:
	+ datafiles with word corpora 'gsw-ch_web_2017_100K-words.txt', 'deu_wikipedia_2016_1M-words.txt', and 'deu-com_web_2018_1M-words.txt' (downloaded from https://corpora.uni-leipzig.de/de#deu). Must be placed in inR/labeling.
- similar cleaning steps as for the labels, creation of one datafile
- output:
	+ preprocessed words from the Leipzig Corpora used for spell checking (leipzig_prepared.csv) 
	

#### frct_labeling_01_preprocessing.R

- R script
- input:
	+ data from labeling questionnaire (frct_labeling_data.csv)
- exclusion of participants with several attempts or too few valid responses, preprocessing of responses (incl. unification of spelling, spell checking with Leipzig corpora,  i.a.), creation of a table with one label per participant and fractal per row
- creation of tables with demographic info, for included and excluded participants
- output:
	+ file with cleaned label per participant and fractal, also includes original responses (frct_labels_cleaned.csv) 
	+ files with demographic data for included participants (demo_labeling_incl.csv) and for all participants (demo_labeling_all.csv)
	

#### frct_labeling_02_indices.R

- R script
- input (read from outR/labeling):
	+ file with demographic data of included participants (demo_labeling_incl.csv)
	+ file with cleaned labels per participant and fractal (frct_labels_cleaned)
- summarize demographic data, descriptive statistics for labels across all fractals
- calculate indices of naming agreement (modal name agreement, h value, d value) and evaluate modal name (most frequent name) per fractal
- descriptive statistics for indices across all fractals
- output: 
	+ violin plots for descriptive statistics
	+ file with naming agreement indices and modal name per fractal (frct_labels_indices.csv)


#### frct_labeling_03_prep_translation.R

- ***THIS STEP MUST BE DONE AT THE END; AFTER fractals_07_external_validations.R***
- R script. The actual translation will be made outside R with an online translator https://www.deepl.com/translator
- input:
	+ file with naming agreement indices (frct_labels_indices.csv)
	+ file with norm data (fractals_norms_01.csv)
- prepare and save word files with modal names and alternative modal names to read into the translator
- paste the translations into a csv document and read it back into R
- add translations to the existing norm data
- output: 
	+ norm table with added translated modal names (fractals_norms_02.csv)


### fractals_03_correlations_rating_labeling.R

- R script
- input:
	+ aggregated rating data (fractals_rating_questionnaire_avg.csv), from outR
	+ naming agreement indices (frct_labels_indices.csv), from outR/labeling
- calculate correlations between ratings and indices of naming agreement
- output: 
	+ word-file with correlation table
	+ scatterplots
	

### fractals_04_alexnet

- subfolder with matlab scripts 
- input:
	+ images from different databases (fractals, Things, Boss, Brodatz). Please find the links to the databases and lists with image names in the subfolder 
- runs the deep neural network (dnn; alexnet) on the images
- output:
	+ file with dnn labeling confidence and labels given by the dnn


### fractals_05_zip_fractals.R

- R script
- input:
	+ fractal images (find the bmp files on osf in 'Fractals_400_380x380_grey_SpecHist_SSIM')
- place the unzipped folder inside inR
- calculates a zip-ratio for each fractal
- output:
	- file with zip-ratios per fractal (zip_fractals.csv)


### fractals_06_recognition.R

- R script
- input:
	+ datafile from recognition memory task (fractals_recognition_data.csv)
- calculates recognition measures per fractal
- output:
	+ file with recognition measures per fractal (reco_dPrime_critC.csv)
	
	
### fractals_07_external_validations.R

- R script
- input: 
	+ averaged rating data (fractals_rating_questionnaire_avg.csv)
	+ indices of naming agreement (frct_labels_indices.csv)
	+ confidences dnn, computed in matlab. name it (confidences_dnn.csv) and place it in outR. You also find the file on OSF (https://osf.io/ckfmv/)
	+ zip ratios (zip_fractals.csv)
	+ recognition measures (reco_dPrime_critC.csv)
- external validations with correlations i.a.
- output:
	+ norm data file (fractals_norms_01.csv)
	+ word files with correlation tables
	+ violin plots, scatterplots, correlations, i.a.

	
### fractals_08_rating_reliablility.R

- R script
- input:
	+ rating data (fractals_rating_questionnaire_data.csv)
- calculates split half reliability based on simulations
